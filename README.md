### 环境要求

Node.js v16.13 及以上版本。

> 从 v16.13 开始，Node.js 发布了 Corepack 来管理包管理器。 这是一项实验性功能，因此您需要通过运行如下脚本来启用它
>
> ```bash
> corepack enable
> ```

### 安装依赖

```bash
# 使用项目package.json中packageManager字段指定的包管理器进行依赖安装
# 若没有指定packageManager字段，将自动添加当前使用的包管理器
pnpm install
```

### 启动项目

```bash
# 启动所有./apps下的项目
pnpm dev
# 启动单个项目 (app-name为./apps下的项目名称)
pnpm dev -F [app-name]
```

### 打包项目

```bash
# 打包所有./apps下的项目
pnpm build
# 打包单个项目 (app-name为./apps下的项目名称)
pnpm build -F [app-name]
```
