export default {
  "*.{js,jsx,vue}": ["prettier --write", "eslint --fix"],
  "*.{scss,less,html,css,md,json}": ["prettier  --write"],
};
