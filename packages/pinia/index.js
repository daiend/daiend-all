const createPersistPlugin = (options = {}) => {
  return function (context) {
    const { store } = context;
    //store.$id
    let storageData;
    // 持久化
    if (context.options.persist) {
      // 加密
      if (context.options.encrypt) {
        storageData = options.encryptStorage.get(store.$id);
      } else {
        storageData = JSON.parse(
          options.normalStorage.getItem(`${options.prefix}_${store.$id}`),
        );
      }
    }
    if (storageData) {
      store.$state = storageData;
    }

    store.$subscribe((mutation, state) => {
      // 是否加密
      if (context.options.persist) {
        if (context.options.encrypt) {
          options.encryptStorage.set(store.$id, state);
        } else {
          options.normalStorage.setItem(
            `${options.prefix}_${store.$id}`,
            JSON.stringify(state),
          );
        }
      }
    });
  };
};

export default createPersistPlugin;
