import path from "path";
import fs from "fs";
// svgstore 使用<symbol>元素将多个SVG文件组合成一个，您可以在标记中<used>这些元素
import store from "svgstore";
// 压缩svg
import { optimize } from "svgo";

const svgFileList = (dir, list = []) => {
  fs.readdirSync(dir).forEach((item) => {
    const fullpath = path.join(dir, item);
    const stats = fs.statSync(fullpath);
    stats.isDirectory() ? svgFileList(fullpath, list) : list.push(fullpath);
  });
  return list;
};

const buildSvgSymbol = (option, filePath) => {
  const fullDir = path.resolve(option.svgDir);
  let symbolId = option.symbolId.replace(
    /\[name\]/g,
    path.parse(filePath).name,
  );
  const dirList = filePath
    .replace(fullDir, "")
    .replace(/\.svg/g, "")
    .split("\\")
    .slice(1, -1);
  return symbolId.replace(/\[dir\]/g, dirList.join("-")).replace("--", "-");
};

function myPlugin(opt = {}) {
  const virtualModuleId = opt.moduleName || "virtual:daiend-svg-sprites";
  const virtualSvgNamesId = opt.svgNames || "virtual:daiend-svg-names";
  const options = {
    symbolId: "icon-[dir]-[name]",
    svgDir: "src/assets/svg",
    inline: false,
    ...opt,
  };

  return {
    name: "daiend-svg-sprites",
    resolveId(id) {
      return id === virtualModuleId || id === virtualSvgNamesId ? id : null;
    },
    load(id) {
      if (id === virtualModuleId) return createSVGSprites(options);
      if (id === virtualSvgNamesId) return creatSvgNames(options);
    },
  };
}

const createSVGSprites = (opt) => {
  const sprites = store(opt);
  const iconsDir = path.resolve(opt.svgDir);
  svgFileList(iconsDir).forEach((item) => {
    const symbolId = buildSvgSymbol(opt, item);
    const code = fs.readFileSync(item, "utf-8");
    sprites.add(symbolId, code);
  });

  const { data: code } = optimize(sprites.toString({ inline: opt.inline }), {
    plugins: [
      "cleanupAttrs",
      "removeDoctype",
      "removeComments",
      "removeTitle",
      "removeDesc",
      "removeEmptyAttrs",
      { name: "removeAttrs", params: { attrs: "(data-name|data-xxx)" } },
    ],
  });

  return `
    let ele = document.createElement('div');
    ele.innerHTML = \`${code}\`;
    ele = ele.getElementsByTagName('svg')[0];
    if (ele) {
      ele.style.position = 'absolute';
      ele.style.width = 0;
      ele.style.height = 0;
      ele.style.overflow = 'hidden';
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (document.body.firstChild) {
        document.body.insertBefore(ele, document.body.firstChild);
      } else {
        document.body.appendChild(ele);
      }
    });
  `;
};

const creatSvgNames = (opt, names = []) => {
  const iconsDir = path.resolve(opt.svgDir);
  svgFileList(iconsDir).forEach((item) => {
    const symbolId = buildSvgSymbol(opt, item);
    names.push(symbolId.replace(/^icon-/g, ""));
  });
  return `export default ${JSON.stringify(names)}`;
};

export { creatSvgNames, createSVGSprites, myPlugin as default };
