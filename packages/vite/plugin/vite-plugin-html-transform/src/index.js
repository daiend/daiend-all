const htmlTitle = (options = {}) => {
  const defaultOptions = {
    title: "admin-vue",
    storage: "localStorage",
    cacheName: "app-cache",
  };
  options = { ...defaultOptions, ...options };
  return {
    name: "html-title",
    enforce: "pre",
    transformIndexHtml(html) {
      html = html.replace(
        /<title>.*<\/title>/,
        `<title>${options.title}</title>`,
      );
      html = html.replace(
        /<div.*<\/div>/,
        `<div id="app">
          <div id="app-init-loading">
            <div class="app-init-loader">
              ${options.title}
            </div>
          </div>
        </div>`,
      );

      return {
        html,
        // 注入标签
        tags: [
          {
            injectTo: "head-prepend",
            tag: "style",
            attrs: { "data-app-loading": "inject-css" },
            children: `
            :root {
              color-scheme: light dark;
            }
            #app-init-loading{
                width:100%;
                height:100vh;
                display:flex;
                justify-content:center;
                align-items:center;
                }
              .app-init-loader {
                width: fit-content;
                font-size: 40px;
                font-family: system-ui, sans-serif;
                font-weight: bold;
                text-transform: uppercase;
                color: #0000;
                -webkit-text-stroke: 1px var(--el-color-primary);
                background: conic-gradient(var(--el-color-primary) 0 0) 50%/0 100%
                  no-repeat text;
                animation: l4 1.5s linear infinite;
              }
              @keyframes l4 {
                to {
                  background-size: 120% 100%;
                }
            }
            `,
          },
          // {
          //   injectTo: "body-prepend",
          //   tag: "div",
          //   attrs: { id: "app-init-loading" },
          //   children: [
          //     {
          //       tag: "div",
          //       attrs: { class: "app-init-loader" },
          //       children: options.title,
          //     },
          //   ],
          // },
          {
            // 放到 <head> 之前，可取值还有`body`|`body-prepend`|`head`|`head-prepend`，
            injectTo: "head-prepend",
            // 标签名
            tag: "script",
            // 标签属性定义
            attrs: { "data-app-loading": "inject-js" },
            // children:`
            children: `
            var theme =null
            if('${options.storage}'==='localStorage'){
               theme = localStorage.getItem('${options.cacheName}');
            }else{
               theme = sessionStorage.getItem('${options.cacheName}');
            }
            document.documentElement.classList.toggle('dark', /dark/.test(theme));`,
          },
        ],
      };
    },
  };
};
export default htmlTitle;
