import chalk from "chalk";

/**
 * Prints custom options to the console after the server has started.
 *
 * @param {object} options - Custom options to be printed. {key: value}
 * @returns {object} - Vite plugin object.
 */
const serverPrint = (options = {}) => {
  return {
    enforce: "pre",
    name: "server-print",
    configureServer(server) {
      const _printUrls = server.printUrls;
      server.printUrls = () => {
        _printUrls();

        for (const [key, value] of Object.entries(options)) {
          console.log(
            `  ${chalk.green("➜")}  ${chalk.bold(key)}: ${chalk.cyan(value)}`,
          );
        }
      };
    },
  };
};
export default serverPrint;
