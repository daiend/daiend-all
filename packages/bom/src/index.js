/**
 * 是否是暗黑模式
 */
export const isDark = window.matchMedia("(prefers-color-scheme:dark)").matches;

/**
 * 是否全屏
 */
export const isFullScreen = document.fullscreenElement !== null;

/**
 * 切换全屏
 */
export const toggleFullScreen = () => {
  if (document.fullscreenElement) {
    document.exitFullscreen();
  } else {
    document.documentElement.requestFullscreen();
  }
};
