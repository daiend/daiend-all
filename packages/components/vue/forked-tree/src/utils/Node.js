import { markNodeData } from "./util";
let nodeIdSeed = 0;

export default class Node {
  constructor(options, isLeftChild = false) {
    this.isLeftChild = isLeftChild;
    this.id = nodeIdSeed++;
    this.data = null;
    this.expanded = false;
    this.leftExpanded = false;
    this.isCurrent = false;
    this.visible = true;
    this.parent = null;
    this.level = 0;
    this.childNodes = [];
    this.leftChildNodes = [];

    Object.assign(this, options); // 使用 Object.assign 优化属性赋值

    if (this.parent) {
      this.level = this.parent.level + 1;
    }

    const store = this.store;
    if (!store) {
      throw new Error("[Node]store is required!");
    }
    store.registerNode(this);

    if (this.data) {
      this.setData(this.data, isLeftChild);
      this.expanded = store.defaultExpandAll || !store.showCollapsable;
      this.leftExpanded = this.expanded;
    }

    if (!Array.isArray(this.data)) {
      markNodeData(this, this.data);
    }

    if (!this.data) return;

    const { defaultExpandedKeys, key } = store;
    if (key && defaultExpandedKeys?.includes(this.key)) {
      this.expand(null, true);
    }

    if (
      key &&
      store.currentNodeKey !== undefined &&
      this.key === store.currentNodeKey
    ) {
      store.currentNode = this;
      store.currentNode.isCurrent = true;
    }

    this.updateLeafState();
  }

  // 节点的展开
  expand(callback, expandParent) {
    const done = () => {
      if (expandParent) {
        let parent = this.parent;
        while (parent.level > 0) {
          parent.isLeftChild
            ? (parent.leftExpanded = true)
            : (parent.expanded = true);
          parent = parent.parent;
        }
      }
      this.isLeftChild ? (this.leftExpanded = true) : (this.expanded = true);
      callback?.();
    };
    done();
  }

  updateLeafState() {
    if (
      this.store.lazy &&
      this.loaded !== true &&
      typeof this.isLeafByUser !== "undefined"
    ) {
      this.isLeaf = this.isLeafByUser;
      return;
    }

    const childNodes = this.childNodes;
    this.isLeaf =
      !this.store.lazy || (this.store.lazy && this.loaded)
        ? !childNodes.length
        : false;
  }
}
