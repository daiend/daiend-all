import Node from "./Node";

export default class Store {
  constructor(options) {
    this.currentNode = null;
    this.currentNodeKey = null;

    Object.assign(this, options); // 使用 Object.assign 优化属性赋值

    this.root = new Node(
      {
        data: this.data,
        store: this,
      },
      false,
    );

    if (this.root.store.onlyBothTree) {
      if (!this.leftData) {
        throw new Error("[Tree] leftData is required in onlyBothTree");
      }

      this.isLeftChilds = new Node(
        {
          data: this.leftData,
          store: this,
        },
        true,
      );

      if (this.isLeftChilds) {
        const leftChildNode = this.isLeftChilds.childNodes[0];
        if (leftChildNode) {
          this.root.childNodes[0].leftChildNodes = leftChildNode.childNodes;
          this.root.childNodes[0].leftExpanded = leftChildNode.leftExpanded;
        }
      }
    }
  }
}
