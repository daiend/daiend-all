const designWidth = 1920;
const designHeight = 1080;
export const baseSize = 16;

// px转vh
export const px2vh = (value, design = designHeight) => {
  if (value.includes("px")) {
    value = value.replaceAll("px", "");
  }
  return `${(value * 100) / design}vh`;
};
// px转vw
export const px2vw = (value, design = designWidth) => {
  if (value.includes("px")) {
    value = value.replaceAll("px", "");
  }
  return `${(value * 100) / design}vw}`;
};
// px转rem
export const px2rem = (value, base = baseSize) => {
  if (value.includes("px")) {
    value = value.replaceAll("px", "");
  }
  return `${value / base}rem`;
};

// px转缩放值
export const px2value = (value, params = { designWidth, designHeight }) => {
  const root = document.documentElement;
  const viewportWidth = window.innerWidth || root.clientWidth;
  const viewportHeight = window.innerHeight || root.clientHeight;
  const scaleWidth = viewportWidth / params.designWidth;
  const scaleHeight = viewportHeight / params.designHeight;
  const scale = Math.min(scaleWidth, scaleHeight);
  return Number((value * scale).toFixed(3));
};
