import { AES, enc, mode, pad } from "crypto-js";
/*
 * @Author: daiend
 * @Date: 2024-02-29 10:48:00
 * @Last Modified by: daiend
 * @Last Modified time: 2024-02-29 10:48:00
 * @Description: 加密类
 */
// AES简介
// 最常见的对称加密算法：高级加密标准(Advanced Encryption Standard)
// 对称加密算法：加密和解密用相同的密钥

export const CIPHER = {
  key: "1234567890123456",
  iv: "1234567890123456",
};
export class AesEncryption {
  constructor(cipher = CIPHER) {
    this.key = cipher.key;
    this.iv = cipher.iv;
  }
  /**
   * 解析字符串
   * @param value
   */
  parse(value) {
    return enc.Utf8.parse(value);
  }
  /**
   * 获取加密参数
   * 加密模式(ECB)：用相同的密钥分别对明文分组加密
   * 填充方式(Padding) ：由于密钥只能对确定长度的数据块进行处理，而数据的长度通常是可变的，
   *                    因此需要对最后一块做额外处理，在加密前进行数据填充
   * 初始向量(iv:Initialization Vector)：目的是防止同样的明文块，始终加密成同样的密文块
   */
  getOptions() {
    return {
      mode: mode.ECB,
      padding: pad.Pkcs7,
      iv: this.parse(this.iv),
    };
  }

  /**
   * 加密
   * @param srcText 待加密明文
   * @returns 加密后的密文
   */
  aseEncrypt(srcText) {
    const encrypted = AES.encrypt(
      srcText,
      this.parse(this.key),
      this.getOptions(),
    );
    this.aseDecrypt(encrypted.ciphertext.toString(enc.Base64));
    return encrypted.ciphertext.toString(enc.Base64);
  }
  /**
   * 解密
   * @param cipherText 待解密密文
   * @returns 解密后的明文
   */
  aseDecrypt(cipherText) {
    const decrypt = AES.decrypt(
      cipherText,
      this.parse(this.key),
      this.getOptions(),
    );
    return decrypt.toString(enc.Utf8);
  }
}

export default AesEncryption;
