/**
 * event emitter / pubsub.
 */

export const mitt = (all) => {
  all = all || new Map();

  return {
    //事件名称注册的处理程序函数
    all,

    /**
     * 为给定类型注册事件处理程序。.
     * @param {string|symbol} type `'*'` for all events
     * @param {Function} handler 响应给定事件而调用的函数
     */
    on(type, handler) {
      const handlers = all.get(type);
      if (handlers) {
        handlers.push(handler);
      } else {
        all.set(type, [handler]);
      }
    },

    /**
     * 删除给定类型的事件处理程序。
     * 如果省略了“handler”，则将删除所有类型的所有处理程序。
     * @param {string|symbol} type 要从中注销“handler”的事件类型（“*”用于删除通配符处理程序）
     * @param {Function} [handler] 要删除的处理程序函数
     */
    off(type, handler) {
      const handlers = all.get(type);
      if (handlers) {
        if (handler) {
          handlers.splice(handlers.indexOf(handler) >>> 0, 1);
        } else {
          all.set(type, []);
        }
      }
    },

    /**
     * 调用给定类型的所有处理程序。
     * 类型匹配的处理程序之后调用“*”处理程序。
     * 注意：不支持手动启动“*”处理程序。
     * @param {string|symbol} type 要调用的事件类型
     * @param {Any} [evt] 任何值（推荐使用强大的对象），传递给每个处理程序
     */
    emit(type, evt) {
      let handlers = all.get(type);
      if (handlers) {
        handlers.slice().map((handler) => {
          handler(evt);
        });
      }

      handlers = all.get("*");
      if (handlers) {
        handlers.slice().map((handler) => {
          handler(type, evt);
        });
      }
    },
  };
};
// 全局通信

export const emitter = mitt();
export default emitter;
