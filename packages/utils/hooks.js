// 设计稿的宽度和高度
const designWidth = 1920;
// 设计稿的高度
const designHeight = 1080;
// 1920x*1080的屏幕分辨率根字体大小
const remBase = 16;
export const useRem = (params = { designWidth, designHeight }) => {
  const root = document.documentElement;
  const setRem = () => {
    const viewportWidth = window.innerWidth || root.clientWidth;
    const viewportHeight = window.innerHeight || root.clientHeight;
    const scaleWidth = viewportWidth / params.designWidth;
    const scaleHeight = viewportHeight / params.designHeight;
    const scale = Math.min(scaleWidth, scaleHeight);
    const rem = remBase * scale;
    root.style.fontSize = rem + "px";
  };
  // 初始化
  setRem();
  // 监听窗口变化
  window.addEventListener("resize", () => {
    setRem();
  });
};
