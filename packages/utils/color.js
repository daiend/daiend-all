/**
 * hex 颜色转 rgb 颜色
 * @param {string} str 颜色值字符串
 * @returns {Array} rgb 颜色值
 */
export const hexToRgb = (str) => {
  let hexs = "";
  let reg = /^\#?[0-9A-Fa-f]{6}$/;
  if (!reg.test(str)) {
    console.warn("输入错误的hex");
    return "";
  }
  str = str.replace("#", "");
  hexs = str.match(/../g);
  for (let i = 0; i < 3; i++) hexs[i] = parseInt(hexs[i], 16);
  return hexs;
};

/**
 * rgb 颜色转 Hex 颜色
 * @param {number} r 红色
 * @param {number} g 绿色
 * @param {number} b 蓝色
 * @returns {string} Hex 颜色
 */
export const rgbToHex = (r, g, b) => {
  let reg = /^\d{1,3}$/;
  if (!reg.test(r) || !reg.test(g) || !reg.test(b)) {
    console.warn("输入错误的rgb颜色值");
    return "";
  }
  let hexs = [r.toString(16), g.toString(16), b.toString(16)];
  for (let i = 0; i < 3; i++) if (hexs[i].length == 1) hexs[i] = `0${hexs[i]}`;
  return `#${hexs.join("")}`;
};

/**
 * 混合颜色
 * @param {string} color1 颜色1
 * @param {string} color2 颜色2
 * @param {number} weight1 权重1 $weight 必须是介于 0% 和 100%（含）之间的数字。
 *                      重量越大，表示应使用较多的 $color1，重量越小，表示应使用较多的 $color2。
 * @returns {string} 混合颜色
 */
export function mixColors(color1, color2, weight1) {
  // 将颜色转换为 RGB 并解析
  const [r1, g1, b1] = color1.match(/\w\w/g).map((hex) => parseInt(hex, 16));
  const [r2, g2, b2] = color2.match(/\w\w/g).map((hex) => parseInt(hex, 16));

  // 计算混合颜色的 RGB
  const r = Math.round(weight1 * r1 + (1 - weight1) * r2);
  const g = Math.round(weight1 * g1 + (1 - weight1) * g2);
  const b = Math.round(weight1 * b1 + (1 - weight1) * b2);

  // 返回混合颜色的 HEX 字符串
  return `#${r.toString(16).padStart(2, "0")}${g.toString(16).padStart(2, "0")}${b.toString(16).padStart(2, "0")}`;
}
