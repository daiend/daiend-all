/**
 * @description 判断是否未定义
 * @param value
 * @returns boolean
 */
export const isUnDef = (value) => {
  return typeof value === "undefined";
};

/**
 * @description 判断是否为null
 * @param value
 * @returns boolean
 */
export function isNull(value) {
  return value === null;
}

/**
 * @description 判断是否为null或undefined
 * @param value
 * @returns boolean
 */
export const isNullOrUnDef = (value) => {
  return isUnDef(value) || isNull(value);
};

/**
 * @function notVoid
 * @desc 校验值是否非空
 * @param {any} val - 校验的值
 * @returns {boolean} - 返回校验结果
 */
export const isNotVoid = (val) => {
  return !["", null, undefined].includes(val);
};
