/**
 * 切换暗黑模式
 * @param {boolean} val 是否开启
 * @param {string} className 类名
 */
export const toggleDark = (val, className = "dark") => {
  const root = document.documentElement;
  if (!root) {
    return;
  }
  root.classList.toggle(className, val);
};
/*
 * 设置主题色
 * @param {string} color 颜色值
 * @param {string} key 变量名
 */
export const setThemeColor = (color, key = "--el-color-primary") => {
  const root = document.documentElement;
  if (!root) {
    return;
  }
  root.style.setProperty(key, color);
};

export const toggleFilter = (key, value) => {
  const root = document.documentElement;
  if (!root) {
    return;
  }
  root.style.setProperty(key, value);
};
