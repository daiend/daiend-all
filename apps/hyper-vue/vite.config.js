import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "node:path";

export default defineConfig({
  base: "/",
  plugins: [vue()],
  server: {
    host: "0.0.0.0",
    port: 9090,
    open: true,
  },
  resolve: {
    alias: {
      // eslint-disable-next-line no-undef
      "@": resolve(process.cwd(), "./src"),
    },
  },
});
