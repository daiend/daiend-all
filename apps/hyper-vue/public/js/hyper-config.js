!(function () {
  var config = JSON.parse(sessionStorage.getItem("__HYPER_CONFIG__")) || {
    theme: "light",
    nav: "vertical",
    layout: { mode: "fluid", position: "fixed" },
    topbar: { color: "light" },
    menu: { color: "dark" },
    sidenav: { size: "default", user: false },
  };
  var html = document.documentElement;

  config.theme = html.getAttribute("data-bs-theme") || config.theme;
  config.nav =
    html.getAttribute("data-layout") === "topnav" ? "horizontal" : config.nav;
  config.layout.mode =
    html.getAttribute("data-layout-mode") || config.layout.mode;
  config.layout.position =
    html.getAttribute("data-layout-position") || config.layout.position;
  config.topbar.color =
    html.getAttribute("data-topbar-color") || config.topbar.color;
  config.sidenav.size =
    html.getAttribute("data-sidenav-size") || config.sidenav.size;
  config.sidenav.user =
    html.getAttribute("data-sidenav-user") !== null || config.sidenav.user;
  config.menu.color = html.getAttribute("data-menu-color") || config.menu.color;

  window.defaultConfig = config;
  window.config = config;

  html.setAttribute("data-bs-theme", config.theme);
  html.setAttribute("data-layout-mode", config.layout.mode);
  html.setAttribute("data-menu-color", config.menu.color);
  html.setAttribute("data-topbar-color", config.topbar.color);
  html.setAttribute("data-layout-position", config.layout.position);
  html.setAttribute("data-sidenav-size", config.sidenav.size);
  config.sidenav.user
    ? html.setAttribute("data-sidenav-user", true)
    : html.removeAttribute("data-sidenav-user");
})();
