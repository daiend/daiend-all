import { defineConfig } from "vite";
import { resolve } from "node:path";
import vue from "@vitejs/plugin-vue";
import serverPrint from "vite-plugin-server-print";
import htmlTransform from "vite-plugin-html-transform";
import svgSprites from "vite-plugin-svg-sprites";
import { appTitle } from "./src/config/app";
import { storageType, piniaPersistPrefix } from "./src/config/app";
import { themeStoreKey } from "./src/config/theme";
// eslint-disable-next-line no-unused-vars
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      vue(),
      serverPrint({
        Doc: "https://gitee.com/daiend/daiend-all/blob/master/README.md",
      }),
      htmlTransform({
        title: appTitle,
        storage: storageType,
        cacheName: `${piniaPersistPrefix}-${themeStoreKey}`,
      }),
      svgSprites({
        symbolId: "icon-[dir]-[name]",
        svgDir: "src/assets/svg",
      }),
    ],
    resolve: {
      alias: {
        // eslint-disable-next-line no-undef
        "@": resolve(process.cwd(), "./src"),
      },
    },
    css: {
      preprocessorOptions: {
        scss: { api: "modern-compiler" },
      },
    },
    server: {
      port: 3000,
      host: "0.0.0.0",
      open: true,
    },
  };
});
