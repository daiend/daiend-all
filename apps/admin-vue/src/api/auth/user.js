import { token, userInfo } from "@/mock/user";

export const getTokenApi = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        code: 200,
        data: token,
      });
    }, 1000);
  });
};
export const getUserInfoApi = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        code: 200,
        data: userInfo,
      });
    }, 1000);
  });
};
