import login from "./modules/login";
import register from "./modules/register";
import menu from "./modules/menu";
import lock from "./modules/lock";
import userNav from "./modules/userNav";
import dialog from "./modules/dialog";
import drawer from "./modules/drawer";
import settings from "./modules/settings";
import button from "./modules/button";
import form from "./modules/form";
import organization from "./modules/organization";
import resource from "./modules/resource";
const zhCn = {
  login,
  register,
  menu,
  lock,
  userNav,
  dialog,
  drawer,
  settings,
  button,
  form,
  organization,
  resource,
};

export default zhCn;
