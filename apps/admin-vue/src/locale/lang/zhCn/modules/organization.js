const organization = {
  name: "名称",
  address: "地址",
  sort: "排序",
  status: "状态",
  description: "描述",
  createTime: "创建时间",
  updateTime: "修改时间",
  remark: "备注",
  operation: "操作",
};
export default organization;
