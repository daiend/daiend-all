const login = {
  title: "系统登录",
  username: "用户名",
  password: "密码",
  verify: "向右滑动验证",
  verifySuccess: "验证通过",
  remember: "记住密码",
  forgetPassword: "忘记密码?",
  submit: "登录",
  usernamePlaceholder: "请输入用户名",
  passwordPlaceholder: "请输入密码",
  usernameRequired: "请输入用户名",
  passwordRequired: "请输入密码",
  captchaRequired: "请完成滑动验证",
  noAccount: "没有账号?",
  register: "注册",
};

export default login;
