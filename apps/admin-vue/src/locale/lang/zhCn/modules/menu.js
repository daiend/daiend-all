const menu = {
  index: "首页",
  home: "主页",
  dashboard: "仪表盘",
  analytics: "分析",
  workspace: "工作台",
  system: "系统管理",
  organization: "组织管理",
  enterprise: "企业管理",
  department: "部门管理",
  post: "岗位管理",
  structure: "组织架构",
  resource: "资源管理",
  menu: "菜单管理",
  button: "按钮管理",
  resStrcuture: "资源结构",
};

export default menu;
