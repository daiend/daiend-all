const register = {
  title: "注册",
  usernamePlaceholder: "用户名",
  emailPlaceholder: "邮箱",
  passwordPlaceholder: "密码",
  confirmPasswordPlaceholder: "确认密码",
  slideToVerify: "滑动验证",
  agreeTermsAlert: "同意服务条款",
  submit: "提交",
  usernameRequired: "用户名是必填项",
  emailRequired: "邮箱是必填项",
  passwordRequired: "密码是必填项",
  confirmPasswordRequired: "确认密码是必填项",
  captchaRequired: "请完成滑动验证",
  hasAccount: "已有账号?",
  login: "登录",
};

export default register;
