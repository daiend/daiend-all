const userNav = {
  account: "我的账号",
  help: "帮助",
  lock: "锁屏",
  logout: "注销",
  lockTip: "您确定要锁屏吗？",
  logoutTip: "您确定要注销登录吗？",
  ok: "确定",
  cancel: "取消",
  warning: "警告",
};
export default userNav;
