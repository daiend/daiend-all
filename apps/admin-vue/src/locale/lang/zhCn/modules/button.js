const button = {
  add: "添加",
  import: "导入",
  export: "导出",
  edit: "编辑",
  delete: "删除",
  search: "搜索",
  reset: "重置",
  query: "查询",
  cancel: "取消",
  confirm: "确定",
  save: "保存",
  close: "关闭",
  submit: "提交",
  update: "修改",
  view: "查看",
  setting: "设置",
};
export default button;
