const resource = {
  path: "路径",
  name: "名称",
  redirect: "重定向路径",
  component: "组件路径",
  sort: "排序",
  status: "状态",
  description: "描述",
  createTime: "创建时间",
  updateTime: "更新时间",
  remark: "备注",
  operation: "操作",
  icon: "图标",
};
export default resource;
