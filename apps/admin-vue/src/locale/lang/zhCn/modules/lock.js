const lock = {
  title: "请输入密码解锁系统",
  passwordPlaceholder: "密码",
  passwordRequired: "请输入密码",
  submit: "解锁",
  captchaRequired: "请完成滑块验证",
};
export default lock;
