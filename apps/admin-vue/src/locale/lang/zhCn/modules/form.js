const form = {
  inputPlaceholder: "请输入",
  selectPlaceholder: "请选择",
  selectPlaceholderMultiple: "请选择（可多选）",
  datePlaceholder: "请选择日期",
  dateRangePlaceholder: "请选择日期范围",
  timePlaceholder: "请选择时间",
  timeRangePlaceholder: "请选择时间范围",
};
export default form;
