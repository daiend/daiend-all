const resource = {
  path: "Path",
  name: "Name",
  redirect: "Redirect",
  component: "Component",
  sort: "Sort",
  status: "Status",
  description: "Description",
  createTime: "Create Time",
  updateTime: "Update Time",
  remark: "Remark",
  operation: "Operation",
  icon: "Icon",
};

export default resource;
