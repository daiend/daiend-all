const register = {
  title: "Register",
  usernamePlaceholder: "Username",
  emailPlaceholder: "Email",
  passwordPlaceholder: "Password",
  confirmPasswordPlaceholder: "Confirm password",
  slideToVerify: "Slide to verify",
  agreeTermsAlert: "Please agree to the terms of service",
  submit: "Submit",
  usernameRequired: "Username is required",
  emailRequired: "Email is required",
  passwordRequired: "Password is required",
  confirmPasswordRequired: "Confirm password is required",
  captchaRequired: "Please complete the slider verification",
  hasAccount: "Already have account?",
  login: "Log In",
};

export default register;
