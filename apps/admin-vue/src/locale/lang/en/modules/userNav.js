const userNav = {
  account: "My Account",
  help: "Help",
  lock: "Lock screen",
  logout: "Logout",
  lockTip: "Are you sure you want to lock the screen?",
  logoutTip: "Are you sure you want to log out?",
  ok: "OK",
  cancel: "Cancel",
  warning: "Warning",
};
export default userNav;
