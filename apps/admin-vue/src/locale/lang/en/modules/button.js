const button = {
  add: "Add",
  import: "Import",
  export: "Export",
  edit: "Edit",
  delete: "Delete",
  search: "Search",
  reset: "Reset",
  query: "Query",
  cancel: "Cancel",
  confirm: "Confirm",
  save: "Save",
  close: "Close",
  submit: "Submit",
  update: "Update",
  view: "View",
  setting: "Setting",
};
export default button;
