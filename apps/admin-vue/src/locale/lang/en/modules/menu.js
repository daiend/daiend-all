const menu = {
  index: "Index",
  home: "Home",
  dashboard: "Dashboard",
  analytics: "Analytics",
  workspace: "Workspace",
  system: "System",
  organization: "Organization",
  enterprise: "Enterprise",
  department: "Department",
  post: "Post",
  structure: "Structure",
  resource: "System Resource",
  menu: "Menu",
  button: "Button",
  resStrcuture: "Resource Structure",
};

export default menu;
