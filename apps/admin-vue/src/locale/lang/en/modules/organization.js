const organization = {
  name: "Name",
  address: "Address",
  sort: "Sort",
  status: "Status",
  description: "Description",
  createTime: "Create Time",
  updateTime: "Update Time",
  remark: "Remark",
  operation: "Operation",
};
export default organization;
