const lock = {
  title: "Enter your password to access the admin.",
  passwordPlaceholder: "Password",
  submit: "Unlock",
  captchaRequired: "Please complete the slider verification",
  passwordRequired: "Please input password",
};
export default lock;
