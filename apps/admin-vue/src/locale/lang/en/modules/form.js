const form = {
  inputPlaceholder: "Place Enter",
  selectPlaceholder: "Please Select",
  selectPlaceholderMultiple: "Please Select (Multiple)",
  datePlaceholder: "Please Select Date",
  dateRangePlaceholder: "Please Select Date Range",
  timePlaceholder: "Please Select Time",
  timeRangePlaceholder: "Please Select Time Range",
};

export default form;
