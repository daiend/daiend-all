const login = {
  title: "System Login",
  username: "Username",
  password: "Password",
  verify: "Slide to verify",
  verifySuccess: "Verification passed",
  remember: "Remember me",
  forgetPassword: "Forget password?",
  submit: "Login",
  usernamePlaceholder: "Please input username",
  passwordPlaceholder: "Please input password",
  usernameRequired: "Please input username",
  passwordRequired: "Please input password",
  captchaRequired: "Please complete the slider verification",
  noAccount: "Don't have an account?",
  register: "Sign Up",
};

export default login;
