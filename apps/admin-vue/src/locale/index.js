import { createI18n } from "vue-i18n";
import zhCn from "./lang/zhCn";
import en from "./lang/en";
import { useAppInit } from "../hooks/init";

const messages = {
  zhCn,
  en,
};
const i18n = createI18n({
  legacy: false,
  locale: useAppInit().appStore.locale,
  fallbackLocale: "zhCn",
  messages,
});

export default i18n;
