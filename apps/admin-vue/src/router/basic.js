let modules = import.meta.glob("../views/**/*.vue");

// 路由白名单
export const WHITE_NAMES = ["login", "register"];
// 静态路由
export const staticRouters = [
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/auth/login.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("@/views/auth/register.vue"),
  },
  {
    path: "/lock",
    name: "lock",
    component: () => import("@/views/auth/lock.vue"),
  },
  {
    path: "/profile",
    name: "profile",
    redirect: "/profile/user",
    component: () => import("@/views/layout/special/index.vue"),
    children: [
      {
        path: "/profile/user",
        name: "user",
        component: () => import("@/views/auth/profile.vue"),
      },
    ],
  },

  {
    path: "/template",
    name: "template",
    component: () => import("@/views/template/index.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: () => import("../views/errorPage/404.vue"),
  },
];

// 动态路由
export const buildDynamicRoutes = (menu) => {
  const routers = [];
  menu.forEach((item) => {
    routers.push({
      ...item,
      children: item.children ? buildDynamicRoutes(item.children) : [],
      component: modules[`../views/${item.component}.vue`],
    });
  });
  return routers;
};
