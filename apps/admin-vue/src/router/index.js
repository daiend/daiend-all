import { createRouter, createWebHistory } from "vue-router";
import { WHITE_NAMES, staticRouters, buildDynamicRoutes } from "./basic";
import { useUserStore } from "@/store/modules/userStore";
import { useAppStore } from "@/store/modules/appStore";
import { getUserInfoApi } from "@/api/auth/user";

export const router = createRouter({
  history: createWebHistory(),
  routes: [...staticRouters],
});

// 路由拦截
router.beforeEach(async (to) => {
  //   axiosCanceler.removeAllPendingAxios();
  const userStore = useUserStore();
  const token = userStore?.token;
  const appStore = useAppStore();
  if (!token) {
    if (to.name && WHITE_NAMES.includes(to.name)) {
      return true;
    } else {
      return { path: "/login" };
    }
  } else {
    if (appStore.lock && to.fullPath != "/lock") {
      return { path: "/lock" };
    } else {
      return true;
    }
  }
});

export const installRouter = (app) => {
  return new Promise((resolve, reject) => {
    const userStore = useUserStore();
    if (userStore.token) {
      getUserInfoApi()
        .then((res) => {
          userStore.setUserInfo(res.data || null);
          buildDynamicRoutes(res.data.menu).forEach((route) => {
            router.addRoute(route);
          });
          app.use(router);
          resolve();
        })
        .catch(() => {
          reject();
        });
    } else {
      app.use(router);
      resolve();
    }
  });
};

export default installRouter;
