export const token = "123456";
export const userInfo = {
  username: "admin",
  avatar: "",
  menu: [
    {
      path: "/",
      name: "index",
      redirect: "/home",
      component: "layout/special/index",
      children: [
        {
          path: "/home",
          name: "home",
          component: "home/index",
        },
      ],
    },
    {
      path: "/dashboard",
      name: "dashboard",
      redirect: "/dashboard/analytics",
      component: "layout/index",
      children: [
        {
          path: "/dashboard/analytics",
          name: "analytics",
          component: "dashboard/analytics/index",
        },
        {
          path: "/dashboard/workspace",
          name: "workspace",
          component: "dashboard/workspace/index",
        },
      ],
    },
    {
      path: "/system",
      name: "system",
      redirect: "/system/organization",
      component: "layout/index",
      children: [
        {
          path: "/system/organization",
          name: "organization",
          redirect: "/system/organization/enterprise",
          component: "layout/empty/index",
          children: [
            {
              path: "/system/organization/enterprise",
              name: "enterprise",
              component: "system/organization/enterprise/index",
            },
            {
              path: "/system/organization/department",
              name: "department",
              component: "system/organization/department/index",
            },
            {
              path: "/system/organization/post",
              name: "post",
              component: "system/organization/post/index",
            },
            {
              path: "/system/organization/structure",
              name: "structure",
              component: "system/organization/structure/index",
            },
          ],
        },
        {
          path: "/system/resource",
          name: "resource",
          redirect: "/system/resource/menu",
          component: "layout/empty/index",
          children: [
            {
              path: "/system/resource/menu",
              name: "menu",
              component: "system/resource/menu/index",
            },
            {
              path: "/system/resource/button",
              name: "button",
              component: "system/resource/button/index",
            },
            {
              path: "/system/resource/resStrcuture",
              name: "resStrcuture",
              component: "system/resource/resStrcuture/index",
            },
          ],
        },
      ],
    },
    // {
    //   path: "/user",
    //   name: "user",
    //   component: "user/index",
    // },
  ],
  roles: [],
  permissions: [],
  post: "研发",
};
