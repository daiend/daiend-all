// 企业数据
// 表结构sql
export const enterpriseSql = `
CREATE TABLE enterprise (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL COMMENT '企业名称',
  sort int(11) DEFAULT NULL COMMENT '排序',
  status tinyint(4) DEFAULT NULL COMMENT '状态',
  description varchar(255) DEFAULT NULL COMMENT '描述',
  create_time datetime DEFAULT NULL COMMENT '创建时间',
  update_time datetime DEFAULT NULL COMMENT '更新时间',
  remark varchar(255) DEFAULT NULL COMMENT '备注',
  address varchar(255) DEFAULT NULL COMMENT '地址',
)`;

const enterprise = [
  {
    id: 1,
    name: "戴安企业",
    sort: "0",
    status: 1,
    description: "戴安企业",
    createTime: "2024-11-18",
    updateTime: "2024-11-19",
    remark: "戴安企业",
    address: "北京",
    children: [
      {
        id: 2,
        name: "北京总公司",
        sort: "0",
        status: 1,
        description: "北京总公司",
        createTime: "2024-11-18",
        updateTime: "2024-11-19",
        remark: "北京总公司",
        address: "北京",
      },
      {
        id: 3,
        name: "上海分公司",
        sort: "0",
        status: 1,
        description: "上海分公司",
        createTime: "2024-11-18",
        updateTime: "2024-11-19",
        remark: "上海分公司",
        address: "上海",
      },
    ],
  },
  {
    id: 4,
    name: "上海公司",
    sort: "0",
    status: 1,
    description: "上海分公司",
    createTime: "2024-11-18",
    updateTime: "2024-11-19",
    remark: "上海分公司",
    address: "上海",
    children: [
      {
        id: 5,
        name: "上海分公司-1",
        sort: "0",
        status: 1,
        description: "上海分公司-1",
        createTime: "2024-11-18",
        updateTime: "2024-11-19",
        remark: "上海分公司-1",
        address: "",
      },
    ],
  },
];

export default enterprise;
