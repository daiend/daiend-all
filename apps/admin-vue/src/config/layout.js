export const layoutStoreKey = "layout";

// 登录、注册布局 center、start、end
export const authLayout = "center";
// 应用布局  vertical、horizontal
export const appLayout = "vertical";
// 布局模式  fixed、fluid
export const appLayoutMode = "fluid";

export const fixedWidth = "1200px";
// 头部高度
export const headerHeight = "50px";
// 标签页高度
export const tabHeight = "40px";
// 侧边栏宽度
export const asideWidth = "200px";
// 主内容区域宽度
// 主内容区域内边距
export const mainPadding = "20px";

export const asideCollapsed = false;

// 侧边栏折叠宽度
export const asideCollapsedWidth = "60px";

export const configLayoutStore = {
  authLayout,
  appLayout,
  appLayoutMode,
  fixedWidth,
  headerHeight,
  tabHeight,
  asideWidth,
  mainPadding,
  asideCollapsed,
  asideCollapsedWidth,
};
