// componentStore key
export const providerStoreKey = "provider";
// 尺寸 default、small、large
export const size = "default";
// 默认命名空间
export const namespace = "el";
// 弹窗层级
export const zIndex = 2000;
// 消息配置
export const message = {
  max: 3, // 最大显示数量
  grouping: true, // 是否合并内容相同的消息
  duration: 3000, // 显示时间
  showClose: false, // 是否显示关闭按钮
  offset: 16, // 顶部的偏移量
};
// 设置 empty-values 来配置组件的默认空值。 默认值是 ['', null, undefined]。
// 如果认为空字符串不是一个空值，可以设置成 [undefined, null]。
export const emptyValues = ["", null, undefined];
// 设置 value-on-clear 以设置清空选项的值。 组件默认值是 undefined。 在日期组件中是 null。
// 如果想设置成 undefined，请使用 () => undefined。
export const valueOnClear = () => null;

export const configProviderStore = {
  namespace,
  size,
  zIndex,
  message,
  emptyValues,
  valueOnClear,
};
