export const themeStoreKey = "theme";
// 模式 light、dark ""
export const mode = "";
// 颜色
export const color = "#409EFF";
// css滤镜 grayscale 、sepia、saturate、hue-rotate、invert、opacity、brightness、contrast、drop-shadow、blur
export const filter = "";

export const defaultColors = [
  {
    name: "blue",
    value: "#409EFF",
  },
  {
    name: "green",
    value: "#67C23A",
  },
  {
    name: "orange",
    value: "#E6A23C",
  },
  {
    name: "red",
    value: "#F56C6C",
  },
  {
    name: "grey",
    value: "#909399",
  },
];

export const configThemeStore = {
  mode,
  color,
  filter,
};
