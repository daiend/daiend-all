// 应用标题
export const appTitle = "admin-vue";

// 存储 默认localStorage | sessionStorage
export const storageType = "localStorage";
// pinia持久化key
export const piniaPersistPrefix = "admin-vue";
// piniaStore key
export const appStoreKey = "app";
// 语言 zhCn、en
export const locale = "zhCn";
// 是否锁定
export const lock = false;
// 是否全屏
export const fullScreen = false;

// 应用配置Store
export const configAppStore = {
  appTitle,
  locale,
  lock,
  fullScreen,
};
