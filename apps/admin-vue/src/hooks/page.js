import NProgress from "nprogress";
import { watch, onUnmounted } from "vue";
import { useRefreshStore } from "@/store/modules/refreshStore";
NProgress.configure({ showSpinner: false });
export const useNProgress = () => {
  NProgress.configure({ showSpinner: false });
  const refreshStore = useRefreshStore();
  refreshStore.loading = false;
  watch(
    () => refreshStore.loading,
    (newVal) => {
      if (newVal) {
        NProgress.start();
      } else {
        NProgress.done();
      }
    },
  );
  onUnmounted(() => {
    NProgress.done();
  });
};
export const useCurrentSizeChange = (currentPage, pageSize, getPageData) => {
  watch(
    [() => currentPage.value, () => pageSize.value],
    ([newCurrent, newSize]) => {
      getPageData({ currentPage: newCurrent, pageSize: newSize });
    },
  );
};
