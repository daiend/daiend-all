import { isDark, isFullScreen } from "bom-utils";
import { toggleDark, setThemeColor, toggleFilter } from "dom-utils";
import {
  storageType,
  piniaPersistPrefix,
  appStoreKey,
  configAppStore,
} from "@/config/app";
import { themeStoreKey, configThemeStore } from "@/config/theme";
import { layoutStoreKey, configLayoutStore } from "@/config/layout";
import { providerStoreKey, configProviderStore } from "@/config/provider";
import { useTheme } from "@/hooks/theme";
// import { AesEncryption } from "common-utils";
// import { WebStorage } from "common-utils";
// // 常规存储
// const normalStorage =
//   storageType === "localStorage" ? localStorage : sessionStorage;
// const encryption = new AesEncryption();
// // 加密存储
// const encryptStorage = new WebStorage({
//   storage: normalStorage,
//   prefix: piniaPersistPrefix,
//   encryption: encryption,
// });
// 初始化app
export const useAppInit = () => {
  // // 常规存储
  // const normalStorage =
  //   storageType === "localStorage" ? localStorage : sessionStorage;
  // const encryption = new AesEncryption();
  // // 加密存储
  // const encryptStorage = new WebStorage({
  //   storage: normalStorage,
  //   prefix: piniaPersistPrefix,
  //   encryption: encryption,
  // });

  const storage =
    storageType === "localStorage" ? localStorage : sessionStorage;

  const getStore = (key, config) => {
    const storageData = storage.getItem(`${piniaPersistPrefix}_${key}`);
    return storageData
      ? { ...config, ...JSON.parse(storageData) }
      : { ...config };
  };

  // 初始化主题
  const themeStore = getStore(themeStoreKey, configThemeStore);
  const dark = themeStore.mode === "dark" || (themeStore.mode === "" && isDark);
  themeStore.mode = dark ? "dark" : "light";
  toggleDark(dark);
  setThemeColor(themeStore.color);
  const { changeThemeLightsColor } = useTheme();
  changeThemeLightsColor(dark, themeStore.color);
  themeStore.filter && toggleFilter(`--filter`, `${themeStore.filter}(1)`);
  storage.setItem(
    `${piniaPersistPrefix}_${themeStoreKey}`,
    JSON.stringify(themeStore),
  );

  // 初始化布局
  const layoutStore = getStore(layoutStoreKey, configLayoutStore);
  storage.setItem(
    `${piniaPersistPrefix}_${layoutStoreKey}`,
    JSON.stringify(layoutStore),
  );

  // 初始化app
  const appStore = getStore(appStoreKey, configAppStore);
  appStore.fullScreen = isFullScreen;
  storage.setItem(
    `${piniaPersistPrefix}_${appStoreKey}`,
    JSON.stringify(appStore),
  );

  // 初始化provider
  const providerStore = getStore(providerStoreKey, configProviderStore);
  storage.setItem(
    `${piniaPersistPrefix}_${providerStoreKey}`,
    JSON.stringify(providerStore),
  );

  return {
    themeStore,
    layoutStore,
    appStore,
    providerStore,
  };
};

export const useRemoveAppInitLoading = () => {
  const loadingElement = document.querySelector("#app-init-loading");
  loadingElement?.remove(); // 移除 loading 元素

  document
    .querySelectorAll('[data-app-loading^="inject"]')
    .forEach((el) => el.remove()); // 移除所有注入的 loading 元素
};
