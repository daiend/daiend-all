import { mixColors } from "common-utils/color";

export const useTheme = () => {
  const changeThemeLightsColor = (isDark, color) => {
    const root = document.documentElement;
    if (!root) {
      return;
    }
    // 需要显示的色阶
    const showList = [3, 5, 7, 8, 9];

    const styles = getComputedStyle(root);
    if (isDark) {
      // 深色模式
      const whiteColor = styles.getPropertyValue("--el-bg-color");
      const blackColor = styles.getPropertyValue("--el-color-white");
      root.style.setProperty(
        "--el-color-primary-dark-2",
        `${mixColors(blackColor, color, 0.2)}`,
      );
      for (let i = 1; i <= 9; i++) {
        if (showList.includes(i)) {
          root.style.setProperty(
            `--el-color-primary-light-${i}`,
            `${mixColors(whiteColor, color, i / 10)}`,
          );
        }
      }
    } else {
      // 浅色模式
      const blackColor = styles.getPropertyValue("--el-color-black");
      const whiteColor = styles.getPropertyValue("--el-color-white");
      root.style.setProperty(
        "--el-color-primary-dark-2",
        `${mixColors(blackColor, color, 0.2)}`,
      );
      for (let i = 1; i <= 9; i++) {
        if (showList.includes(i)) {
          root.style.setProperty(
            `--el-color-primary-light-${i}`,
            `${mixColors(whiteColor, color, i / 10)}`,
          );
        }
      }
    }
  };
  return {
    changeThemeLightsColor,
  };
};
