import { createPinia } from "pinia";
import createPersistPlugin from "pinia-persist-plugin";
import { piniaPersistPrefix, storageType } from "@/config/app";
import { AesEncryption } from "common-utils";
import { WebStorage } from "common-utils";
// 常规存储
const normalStorage =
  storageType === "localStorage" ? localStorage : sessionStorage;
const encryption = new AesEncryption();
// 加密存储
const encryptStorage = new WebStorage({
  storage: normalStorage,
  prefix: piniaPersistPrefix,
  encryption: encryption,
});
const pinia = createPinia();

pinia.use(
  createPersistPlugin({
    prefix: piniaPersistPrefix,
    normalStorage,
    encryptStorage,
  }),
);

export default pinia;
