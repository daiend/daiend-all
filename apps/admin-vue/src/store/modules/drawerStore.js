import { defineStore } from "pinia";
import { ref } from "vue";
export const useDrawerStore = defineStore(
  "drawer",
  () => {
    const drawers = ref([]);
    const open = (name) => {
      if (drawers.value.includes(name)) {
        console.warn("Drawer already open:", name);
        return;
      }
      drawers.value.push(name);
    };
    const close = (name) => {
      drawers.value = drawers.value.filter((n) => n !== name);
    };

    function $reset() {
      drawers.value = [];
    }
    return { drawers, open, close, $reset };
  },
  { persist: true },
);
