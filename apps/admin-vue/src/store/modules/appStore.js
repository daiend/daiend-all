import { appStoreKey, configAppStore } from "@/config/app";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useAppStore = defineStore(
  appStoreKey,
  () => {
    const locale = ref("");
    const lock = ref(false);
    const fullScreen = ref(false);
    const appTitle = ref("");
    const setLocale = (val) => {
      locale.value = val;
    };
    const setLock = (val) => {
      lock.value = val;
    };
    const setFullScreen = (val) => {
      fullScreen.value = val;
    };
    function $reset() {
      locale.value = configAppStore.locale;
      lock.value = configAppStore.lock;
      fullScreen.value = configAppStore.fullScreen;
      if (document.fullscreenElement) {
        document.exitFullscreen();
      }
      appTitle.value = configAppStore.appTitle;
    }
    return {
      locale,
      lock,
      fullScreen,
      appTitle,
      setLocale,
      setLock,
      setFullScreen,
      $reset,
    };
  },
  { persist: true },
);
