import { themeStoreKey, configThemeStore } from "@/config/theme";
import { toggleDark, setThemeColor, toggleFilter } from "dom-utils";
import { isDark } from "bom-utils";
import { useTheme } from "@/hooks/theme";
import { defineStore } from "pinia";
import { ref } from "vue";
const { changeThemeLightsColor } = useTheme();

export const useThemeStore = defineStore(
  themeStoreKey,
  () => {
    const mode = ref("");
    const color = ref("");
    // 颜色滤镜
    const filter = ref("");
    const setMode = (dark) => {
      dark = dark === "" ? isDark : dark;
      mode.value = dark ? "dark" : "light";
      toggleDark(dark);
      changeThemeLightsColor(dark, color.value);
    };
    const setColor = (newColor) => {
      color.value = newColor;
      const isDark = mode.value === "dark";
      changeThemeLightsColor(isDark, newColor);
      setThemeColor(newColor);
    };
    const changeFilter = (value) => {
      filter.value = value;
      if (value) {
        toggleFilter(`--filter`, `${value}(1)`);
      } else {
        toggleFilter(`--filter`, "");
      }
    };
    function $reset() {
      mode.value = configThemeStore.mode;
      color.value = configThemeStore.color;
      filter.value = configThemeStore.filter;
      setMode(mode.value);
      setColor(color.value);
      changeFilter(filter.value);
    }
    return { mode, color, filter, setMode, setColor, changeFilter, $reset };
  },
  { persist: true },
);
