import { defineStore } from "pinia";
import { ref } from "vue";
export const useRefreshStore = defineStore(
  "refresh",
  () => {
    const refresh = ref(null);
    const loading = ref(false);

    const toRefresh = () => {
      refresh.value();
    };
    function $reset() {
      refresh.value = null;
    }
    return { refresh, loading, toRefresh, $reset };
  },
  { persist: true },
);
