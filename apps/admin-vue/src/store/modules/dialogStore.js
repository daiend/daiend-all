import { defineStore } from "pinia";
import { ref } from "vue";
export const useDialogStore = defineStore(
  "dialog",
  () => {
    const dialogs = ref([]);

    const open = (name) => {
      if (dialogs.value.includes(name)) {
        console.warn("Dialog already open:", name);
        return;
      }
      dialogs.value.push(name);
    };
    const close = (name) => {
      dialogs.value = dialogs.value.filter((n) => n !== name);
    };
    function $reset() {
      dialogs.value = [];
    }
    return { dialogs, open, close, $reset };
  },

  { persist: true },
);
