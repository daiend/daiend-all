import { defineStore } from "pinia";
import { ref } from "vue";
import { layoutStoreKey, configLayoutStore } from "@/config/layout";

export const useLayoutStore = defineStore(
  layoutStoreKey,
  () => {
    const authLayout = ref("");
    const appLayout = ref("");
    const appLayoutMode = ref("");
    const fixedWidth = ref("");
    const headerHeight = ref("");
    const tabHeight = ref("");
    const asideWidth = ref("");
    const mainPadding = ref("");
    const asideCollapsed = ref(false);
    const asideCollapsedWidth = ref("");
    const setAuthLayout = (command) => {
      authLayout.value = command;
    };
    const setAppLayoutMode = (command) => {
      appLayoutMode.value = command;
    };
    const setAppLayout = (command) => {
      appLayout.value = command;
    };
    function $reset() {
      authLayout.value = configLayoutStore.authLayout;
      appLayout.value = configLayoutStore.appLayout;
      appLayoutMode.value = configLayoutStore.appLayoutMode;
      fixedWidth.value = configLayoutStore.fixedWidth;
      headerHeight.value = configLayoutStore.headerHeight;
      tabHeight.value = configLayoutStore.tabHeight;
      asideWidth.value = configLayoutStore.asideWidth;
      mainPadding.value = configLayoutStore.mainPadding;
      asideCollapsed.value = configLayoutStore.asideCollapsed;
      asideCollapsedWidth.value = configLayoutStore.asideCollapsedWidth;
    }
    return {
      authLayout,
      appLayout,
      appLayoutMode,
      fixedWidth,
      headerHeight,
      tabHeight,
      asideWidth,
      mainPadding,
      asideCollapsedWidth,
      asideCollapsed,
      setAuthLayout,
      setAppLayout,
      setAppLayoutMode,
      $reset,
    };
  },
  { persist: true },
);
