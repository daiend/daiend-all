import { defineStore } from "pinia";
import { ref } from "vue";
export const useTabStore = defineStore(
  "tab",
  () => {
    const tabList = ref([]);
    const addTab = (tab) => {
      // 添加
      const index = tabList.value.findIndex((item) => item.path === tab.path);
      if (index === -1) {
        if (tabList.value.length >= 7) {
          tabList.value.shift();
        }
        tabList.value.push(tab);
      }
    };

    const closeTab = (tab) => {
      // 关闭
      const index = tabList.value.findIndex((item) => item.path === tab.path);
      tabList.value.splice(index, 1);
    };

    function $reset() {
      tabList.value = [];
    }

    return {
      tabList,
      addTab,
      closeTab,
      $reset,
    };
  },
  { persist: true },
);
