import { providerStoreKey, configProviderStore } from "@/config/provider";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useProviderStore = defineStore(
  providerStoreKey,
  () => {
    const namespace = ref("");
    const size = ref("");
    const zIndex = ref("");
    const message = ref("");
    const emptyValues = ref("");
    const valueOnClear = ref("");
    function $reset() {
      namespace.value = configProviderStore.namespace;
      size.value = configProviderStore.size;
      zIndex.value = configProviderStore.zIndex;
      message.value = configProviderStore.message;
      emptyValues.value = configProviderStore.emptyValues;
      valueOnClear.value = configProviderStore.valueOnClear;
    }
    return {
      namespace,
      size,
      zIndex,
      message,
      emptyValues,
      valueOnClear,
      $reset,
    };
  },
  { persist: true },
);
