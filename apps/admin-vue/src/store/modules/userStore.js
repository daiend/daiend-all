import { defineStore } from "pinia";
import { ref } from "vue";
export const useUserStore = defineStore(
  "user",
  () => {
    const token = ref(null);
    const userInfo = ref(null);
    const setToken = (t) => {
      token.value = t;
    };
    const setUserInfo = (info) => {
      userInfo.value = info;
    };
    function $reset() {
      token.value = null;
      userInfo.value = null;
    }
    return { userInfo, token, setToken, setUserInfo, $reset };
  },
  { persist: true, encrypt: true },
);
