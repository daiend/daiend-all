import { defineConfig } from "vite";
import { resolve } from "node:path";
import vue from "@vitejs/plugin-vue";

// eslint-disable-next-line no-unused-vars
export default defineConfig(({ mode }) => {
  return {
    plugins: [vue()],
    resolve: {
      alias: {
        // eslint-disable-next-line no-undef
        "@/": resolve(process.cwd(), "./src"),
      },
    },
    server: {
      port: 3001,
    },
  };
});
